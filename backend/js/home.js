// 首页的js代码

// 导入ajax
import { axios, isLogin } from '../utils/ajax.js';
// 获取元素
let nameBox = document.querySelector('.top>p>span');
let logoutBtn = document.querySelector('.top>p>button');

(async () => {
  // 请求判断是否登录
  let { status, info, token } = await isLogin();
  if (status != 1) return location.href = './login.html';
  nameBox.innerHTML = info.username;
})()
// 退出注销
logoutBtn.onclick = () => {
  if (!confirm('确定要退出?')) return;
  localStorage.removeItem('token');
  localStorage.removeItem('token');
  location.href = './login.html';
}

// 导入路由
import Routers from "./routers.js";
// 页面的hashchange事件(地址栏中的hash值变化)
window.onhashchange = render;
// 根据页面hash===>对应的组件进行渲染函数
function render() {
  // 获取页面地址中的hash值  
  let hash = location.hash.slice(2);
  // 使用/将hash分隔
  let hashArr = hash.split('/');
  // console.log(location.hash, hash, hashArr) 
  // console.log(Routers);    
  
  // 根据页面的hashArr[0] 找到对应的组件---->一级路由查找
  let router = Routers.find(item => item.name == hashArr[0]);
  // 找不到则切换默认
  if (!router) return location.hash = '/default';


  // 二级路由匹配
  if (hashArr[1]) {
    router = router.children?.find(item => item.name == hashArr[1]) ?? router;
  }
  // console.log(router)

  // 利用组件导出的渲染函数渲染内容--->res接受导入模块导出的内容
  router.component().then(res => res.default())
}


// 页面打开执行一次
render();