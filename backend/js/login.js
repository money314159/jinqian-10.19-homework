// 后台登录js代码
// 导入ajax
import { axios } from '../utils/ajax.js'
// 导入正则函数
import { nameTest, pwdTest } from "../utils/reg.js";


// 获取元素
let nameInp = document.querySelector('.name');
let pwdInp = document.querySelector('.pwd');
let form = document.querySelector('form');

// 点击进行登录请求
form.addEventListener('submit', async e => {
  e.preventDefault();
  // 获取数据
  let name = nameInp.value;
  let pwd = pwdInp.value;

  // 3.1 不为空校验
  if (!name || !pwd) return alert('请完整填写表单');
  // 3.2 正则校验
  if (!nameTest(name)) return alert('用户名格式错误');
  if (!pwdTest(pwd)) return alert('密码格式错误');
  // 4. 发送请求
  // 注意: 请求地址: /users/login 请求方式:post
  //    请求携带参数: username,password
  let { data: { code, token, info } } = await axios({ url: '/user/login', method: 'post', data: { name, pwd } })

  if (code != 1) return alert('用户名或密码错误,请重试') // 登录失败

  // 登录成功---->判断 如果角色不是admin则告知没有权限
  // console.log( info )
  if (info.role != 'admin') return alert('没有后台权限,请联系管理员');

  // 
  // 将用户id和token存储在浏览器本地
  localStorage.setItem('token', token);
  localStorage.setItem('uid', info.id);
  location.href = './home.html';
})
