// 路由模块
// 组装一个路由对照表
let routers = [
  // 一个对象就是一个路由信息--->名称及对应组件
  {
    name: 'home',
    component: () => import('../components/home/home.js')
  },
  { // 一级路由
    name: 'users',
    component: () => import('../components/user/user.js'),
    children: [
      {  // 二级路由
        name: 'list',
        component: () => import('../components/user/children/list/list.js'),
      }
    ]
  },
  {
    name: 'carousel',
    component: () => import('../components/carousel/carousel.js'),
    children: [
      {  // 二级路由
        name: 'list',
        component: () => import('../components/carousel/children/list/list.js'),
      }
    ]
  },
  {
    name: 'goods',
    component: () => import('../components/goods/goods.js'),
    children: [
      {  // 二级路由
        name: 'list',
        component: () => import('../components/goods/children/list/list.js'),
      }, {  // 二级路由
        name: 'add',
        component: () => import('../components/goods/children/add/add.js'),
      }
    ]
  },
  {
    name: 'default',  // 默认组件
    component: () => import('../components/home/home.js')
  },
]


// 导出路由对照表
export default routers;