// 二级路由---->carousel--->list组件
// 轮播图管理 组件
// carouselcarousel--->list组件
console.log('carousel--->list组件导入了');
import { axios } from "../../../../utils/ajax.js";
// 导入html结构及css样式
import { htmlStr, cssStr } from "./view.js";
// 组件的js代码
// 获取路由入口元素
let routerView = document.querySelector('.router-view');
// 渲染
function render() {
  // 轮播图列表渲染
  routerView.innerHTML = htmlStr;
  // css设置
  let styleEle = document.createElement('style');
  styleEle.innerHTML = cssStr;
  document.head.appendChild(styleEle);

  // 获取图片并渲染
  renderUl();
  // 添加
  addFn();
  // 绑定点击事件
  bindEvent()
}

// 渲染已有轮播图
async function renderUl() {
  let ulBox = routerView.querySelector('ul');
  let labelBox = routerView.querySelector('label');
  let { data } = await axios('/carousel/list');
  ulBox.querySelectorAll('li').forEach(ele => ele.remove());
  data.list.forEach(picObj => {
    // 创建li
    let li = document.createElement('li');
    li.className = 'img';
    li.innerHTML = `
      <span class="close" data-name=${picObj.name}>X</span>
      <span class="is_show" data-name=${picObj.name} >${picObj.is_show?'展示':'不展示'}</span>
      <img src=http://127.0.0.1:8888/${picObj.name}>
      `;
    // 添加到ul中
    ulBox.insertBefore(li, labelBox);
  })

}
// 添加轮播图
function addFn() {
  let inp = routerView.querySelector('input');
  let labelBox = routerView.querySelector('label');
  let ulBox = routerView.querySelector('ul');
  // 绑定事件(change)
  inp.onchange = function () {
    // console.log( inp.files );
    // 校验选择的文件必须全部是图片,否则不预览
    let fileArr = [...inp.files];
    // 如果某一个选择的文件不是图片则 return
    if (!fileArr.every(fileObj => fileObj.type.startsWith('image'))) return alert('请选择图片');

    // 遍历选择的文件数组
    fileArr.forEach(async fileObj => {
      // 通过fd 添加图片
      let fd = new FormData();
      fd.set('carousel', fileObj);
      fd.set('is_show', 1);
      let { data } = await axios({
        url: '/carousel/add',
        method: 'post',
        data: fd
      })
      alert(data.message);
      renderUl();
    })

  }
}

function bindEvent() {
  let ulBox = routerView.querySelector('ul');
  // 事件委托移除
  ulBox.onclick = async ({ target }) => {
    if (target.className == 'close') {
      if (!confirm('确定要删除?')) return;
      let name = target.dataset.name;
      let { data } = await axios.get('/carousel/remove', { params: { name } })
      if (data.code != 1) return alert('删除失败');
      renderUl();
    }
    if (target.className == 'is_show') {
      let name = target.dataset.name;
      let val = target.innerText == '不展示'?1:0;
      let { data } = await axios.get('/carousel/update', { params: { name,val } })
      if (data.code != 1) return alert('删除失败');
      renderUl();
      target.innerText == target.innerText == '不展示'?'展示':'不展示';
    }
  }

}


export default render;