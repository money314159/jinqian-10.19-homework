let htmlStr = `
  <h1>文件上传</h1>
  <ul>
    <!-- <li class="img">
      <span class="close">X</span>
      <span class="is_show">不展示</span>
      <img src="./1.webp">
    </li> -->
    <label for="upload">
      +
    </label>
  </ul>
  <input type="file" hidden id="upload">
`;

let cssStr = `
* {
  list-style: none;
}

.router-view {
  width: 1200px;
  border: 2px solid #2ec770;
  border-radius: 20px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
}

.router-view>h1 {
  height: 50px;
}

.router-view>ul {
  flex: 1;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: start;
}

.router-view>ul>li {
  width: 300px;
  height: 150px;
  margin: 10px 20px;
  position: relative;
}

.router-view>ul>li>img {
  width: 100%;
  height: 100%;
  border-radius: 10px;
}

.router-view>ul>li>span.close {
  position: absolute;
  top: 0;
  right: 0;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: #ccc;
  opacity: 0.5;
  text-align: center;
  line-height: 30px;
  cursor: pointer;
}

.router-view>ul>li>span:hover {
  background-color: #f1714f;
  opacity: 0.8;
}


.router-view>ul>li>span.is_show {
  position: absolute;
  bottom: 10px;
  width: 60px;
  height: 30px;
  text-align: center;
  line-height: 30px;
  left: 50%;
  transform: translateX(-50%);
  background-color: #ccc;
  cursor: pointer;
  border-radius: 5px;
  opacity: 0.5;
}

.router-view>ul>li>span.is_show:hover {
  background-color: #16b777;
  opacity: 0.8;
}


.router-view>ul>label {
  margin: 30px 20px;
  width: 100px;
  height: 100px;
  border: 1px solid #000;
  border-radius: 50%;
  font-size: 100px;

  font-weight: bold;
  text-align: center;
  line-height: 90px;
  cursor: pointer;
  vertical-align: baseline;
  background-color: #16b777;
  color: #fff;
}

.router-view>ul>label:hover {
  background-color: #fff;
  color: #000;
}
`

// 导出
export {
  htmlStr,cssStr
}