// 视图模板

// 视图的html结构
let htmlStr = `
<div style='width: 800px;margin-top:15px;'>搜索 <input type="text" style="height:30px;padding-left:10px"></div>
<table border="1" rules=all style="width: 800px; text-align: center;margin-top: 10px;">
  <thead>
    <tr>
      <th>序号</th>
      <th>用户名</th>
      <th>年龄</th>
      <th>性别</th>
      <th>头像</th>
      <th>角色</th>
      <th>重置密码</th>
      <th>禁用操作</th>
      <th>是否删除</th>
    </tr>
  </thead>
  <tbody>
  <!--  
      <tr>
        <td>1</td>
        <td>zs</td>
        <td>17</td>
        <td>男</td>
        <td><img src="http://127.0.0.1:8000/default.jpg" style="width: 50px;height: 50px;"></td>
        <td>admin</td>
        <td><button>重置密码</button></td>
        <td><button>禁用</button></td>
        <td><button>删除</button></td>
      </tr>
    -->
  </tbody>
</table>
<div class="pagination"></div>
`;

// 视图的样式

let htmlCss = {
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'start',
  alignItems: 'center',
  fontSize: '20p',
}


// 导出
export { htmlStr, htmlCss }