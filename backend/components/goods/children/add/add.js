// 二级路由---->goods--->list组件
// 商品管理 组件
// goods-add组件
import { htmlStr, cssStr } from "./view.js";
console.log('goods-add组件导入了');
import{axios} from "../../../../utils/ajax.js"
// 使用字符串模拟html内容
// let strHtml = '<div>goods-add内容</div>';
// 组件的js代码
// 获取路由入口元素
let routerView = document.querySelector('.router-view');
// 渲染
function render() {
  routerView.innerHTML = htmlStr;
  let styleEle = document.createElement('style');
  styleEle.innerHTML = cssStr;
  document.head.appendChild(styleEle);
  bindEvent();

}

function bindEvent() {
  let form = routerView.querySelector('form')
form.onclick= async (e)=>{
  // e.preventDefault()
    let titEle = routerView.querySelector('.tit')
let title = titEle.value
// bigEle.oninput= ()=>{
//   console.log(bigEle.value)
// }
let bigEle = routerView.querySelector('.big')
let smallEle = routerView.querySelector('.small')
let oldEle = routerView.querySelector('.old')
let disEle = routerView.querySelector('.dis')
let numEle = routerView.querySelector('.num')
let hotEle = routerView.querySelector('.hot')
let catEle = routerView.querySelector('.cat')
let introEle = routerView.querySelector('.intro')
let button = routerView.querySelector('button')

let img_big_logo = bigEle.value
let img_small_logo = smallEle.value
let price = oldEle.value
let sale_type = disEle.value
let goods_number = numEle.value
let is_hot =hotEle.checked ? 1 : 0;
// console.dir(hotEle);
// console.log(is_hot)
let category = catEle.value
// console.log(sale_type)
let introduce = introEle.value

if (e.target.innerText==="提交信息"){
  // alert('aaa')
let f = { title,img_big_logo,img_small_logo, price, sale_type, goods_number, is_hot,category, introduce };
// console.log(f)
  let { data } = await axios({ url: '/goods/add', method: 'post', data:f })
 console.log(data.message)
  if ( data.code != 1) return alert('修改失败,请重试');
  }
}
}


export default render;