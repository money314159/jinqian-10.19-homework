// 二级路由---->goods--->list组件
// 商品管理 组件
// goods-list组件
console.log('goods-list组件导入了');
import { htmlStr, cssStr } from "./view.js";
import { axios } from "../../../../utils/ajax.js";
import Pagination from "../../../../utils/pagination.js";
// 组件的js代码
// 获取路由入口元素
let routerView = document.querySelector('.router-view');
// 渲染
function render() {
  routerView.innerHTML = htmlStr;
  // css设置
  let styleEle = document.createElement('style');
  styleEle.innerHTML = cssStr;
  document.head.appendChild(styleEle);
  renderTbody();
  bindEvent();
}

// 分页
let current = 1;
let pagesize = 7;
async function renderTbody() {
  let { data } = await axios({ url: '/goods/list', params: { current, pagesize } })

  // 渲染表格
  routerView.querySelector('tbody').innerHTML = data.list.reduce((prev, item, i) => {
    return prev + `
      <tr>
        <td>${i + 1}</td>
        <td>${item.title}</td>
        <td><img src=${item.img_small_logo} ></td>
        <td>¥ <span contenteditable class='price'>${item.price}</span></td>
        <td>
          <select class='sale_type'>
            <option ${item.sale_type == '10' ? 'selected' : ''} value="10">100%</option>
            <option ${item.sale_type == '9' ? 'selected' : ''} value="9">90%</option>
            <option ${item.sale_type == '8' ? 'selected' : ''} value="8">80%</option>
            <option ${item.sale_type == '7' ? 'selected' : ''} value="7">70%</option>
            <option ${item.sale_type == '6' ? 'selected' : ''} value="6">60%</option>
            <option ${item.sale_type == '5' ? 'selected' : ''} value="5">50%</option>
          </select>
        </td>
        <td >¥ <span class='current_price'>${item.current_price}</span></td>
        <td class="goods_number" contenteditable>${item.goods_number}</td>
        <td><input class='is_hot' type="checkbox" ${item.is_hot ? 'checked' : ''}></td>
        <td><button style='margin-right:8px' data-id=${item._id} >保存</button><button data-id=${item._id}>移除</button></td>
      </tr>    
    `
  }, '')

  routerView.querySelector('.pagination').innerHTML = '';
  // 将分页器的点击事件解绑
  routerView.querySelector('.pagination').onclick = null;
  // 分页器
  new Pagination('.pagination', {
    current,
    pageSize: pagesize,
    totalPage: data.totalPage,
    change(info) {
      // 通过事件对象--获取事件目标
      // console.log(window.event.target.nodeName)
      // 当事件目标是 li元素的时候才下一页渲染
      let nodeName = window.event.target.nodeName
      if (nodeName != 'LI' && nodeName != 'P' && nodeName != 'BUTTON') return;
      current = info.current;
      renderTbody();
    }
  })

  let tbody = routerView.querySelector('tbody');
  tbody.querySelectorAll('.price').forEach(ele => {
    ele.onblur = function () {
      // console.log(this)
      let price = this.innerText;
      let saleTypeEle = this.parentNode.nextElementSibling.firstElementChild
      let sale_type = saleTypeEle.value;
      let curPirceEle = this.parentNode.nextElementSibling.nextElementSibling.firstElementChild
      curPirceEle.innerText = price * (sale_type / 10)
    }
  })
  tbody.querySelectorAll('.sale_type').forEach(ele => {
    ele.onchange = function () {
      // console.log(this)
      let sale_type = this.value;
      let price = this.parentNode.previousElementSibling.firstElementChild.innerText
      let curPirceEle = this.parentNode.nextElementSibling.firstElementChild
      curPirceEle.innerText = price * (sale_type / 10)
    }
  })

}

function bindEvent() {
  let tbody = routerView.querySelector('tbody')
  tbody.onclick = async ({ target }) => {
    if (target.innerText == '保存') {
      let id = target.dataset.id;
      let tr = target.parentNode.parentNode;
      let price = tr.querySelector('.price').innerText;
      let sale_type = tr.querySelector('.sale_type').value;
      let current_price = tr.querySelector('.current_price').innerText;
      let goods_number = tr.querySelector('.goods_number').innerText;
      let is_hot = tr.querySelector('.is_hot').checked ? 1 : 0;
      let is_sale = sale_type == 10 ? 0 : 1
      // console.log(price, sale_type, current_price, goods_number, is_hot, is_sale)
      let data = { id, price, current_price, sale_type, goods_number, is_hot, is_sale };
      let { data: { code } } = await axios({ url: '/goods/update', method: 'post', data })
      if (code != 1) return alert('修改失败,请重试');
      renderTbody();
    }
    if (target.innerText == '移除') {
      if(!confirm('确定要删除吗?')) return ;
      let id = target.dataset.id;
      let { data } = await axios({ url: '/goods/remove', method: 'post', data: { id } })
      if (data.code != 1) return alert('删除失败,请重试');
      renderTbody();
    }

  }


}



export default render;