let htmlStr = `
<h1>商品列表</h1>
<table border="1" rules=all>
  <thead>
    <th>序号</th>
    <th>商品名称</th>
    <th>商品图片</th>
    <th>商品原价</th>
    <th>折扣</th>
    <th>商品现价</th>
    <th>商品库存</th>
    <th>是否热销</th>
    <th>商品移除</th>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>南极人【10双装】夏季男士丝袜超薄中筒袜男袜子薄款透气纯色商务袜P3038</td>
      <td><img src="https://image5.suning.cn/uimg/b2c/newcatentries/0070081143-000000000144879464_2_200x200.jpg"
          alt=""></td>
      <td>¥ <span contenteditable>25.00</span></td>
      <td>
        <select>
          <option value="100">100%</option>
          <option value="90">90%</option>
          <option value="80">80%</option>
          <option value="70">70%</option>
          <option value="60">60%</option>
          <option value="50">50%</option>
        </select>
      </td>
      <td>¥25</td>
      <td class="sale" contenteditable>200</td>
      <td><input type="checkbox"></td>
      <td><button style='margin-right:8px'>保存</button><button>移除</button></td>
    </tr>
  </tbody>
</table>
<div class='pagination'><div>

`


let cssStr = `
.router-view {
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
}

.router-view>table {
  width: 800px;
  border: 1px solid #000;
  text-align: center;
}

.router-view>table img {
  width: 50px;
  height: 50px;
}

.router-view>table>tbody>tr {
  height: 60px;
}

.router-view>table tbody>tr>td:nth-of-type(2) {
  color: red;
  width: 250px;
  font-size: 14px;

}

.router-view>table tbody>tr>td>input[type=checkbox] {
  width: 30px;
  height: 30px;
}


`

export {
  htmlStr,cssStr
}