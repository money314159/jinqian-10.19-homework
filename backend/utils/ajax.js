import axios from "./axios.js";

// 设置axios的基准地址
axios.defaults.baseURL = 'http://127.0.0.1:8888';
// 请求拦截,响应拦截

// 封装一个判断用户是否已登录函数
// 返回值:  成功的promsie对象
//    -- 状态值  {status:0/1,message:'未登录/已登录',info:'用户信息,登录过才可获取到'}
async function isLogin() {
  // 获取本地存储的token和id
  let token = localStorage.getItem('token');
  let id = localStorage.getItem('uid');
  if (!token || !id) return { status: 0, message: '未登录' };
  // 根据获取的token和id 请求/users/info接口
  // 请求方式: get 携带数据: id,请求头: token
  let { data: { code, info } } = await axios({ url: '/user/info', params: { id }, headers: { authorization: token } });
  // 校验是否登录
  if (code != 1) return { status: 0, message: '未登录' };

  if (info.role != 'admin') return { status: 0, message: '没有权限' };
  // 已登录
  return { status: 1, message: '已登录', info, token }
}


// 批量设置元素样式方法
function setStyle(ele, styleObj) {
  for (const type in styleObj) {
    ele.style[type] = styleObj[type];
  }
}


export { axios, isLogin,setStyle };